import style from "./Card.module.scss";
import { Component } from "react";
import Button from "../Button";
import PropTypes from "prop-types";

class Card extends Component {
  render() {
    const {
      header,
      src,
      price,
      color,
      addInCart,
      article,
      cardsInCart,
      addInFav,
      cardsInFav,
    } = this.props;
    return (
      <div className={style.Card}>
        <div
          className={style.star}
          onClick={() => {
            addInFav(article);
          }}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            version="1.1"
            width="20px"
            height="20px"
            fill={cardsInFav ? "#ea2f2f" : "#909090"}
          >
            <polygon points="12,3 6,21 21,9 3,9 18,21" />
          </svg>
        </div>
        <img
          className={style.img}
          src={src}
          alt="img"
          width="250px"
          height="250px"
        />
        <h2 className={style.title}>{header}</h2>
        <p className={style.price}>{price}</p>
        <div className={style.color} style={{ backgroundColor: color }}></div>
        <Button
          backgroundColor={cardsInCart ? "#991212" : "#ea2f2f"}
          text={cardsInCart ? "Added" : "Add to Cart"}
          className={style.btn}
          onClick={() => {
            addInCart(article);
          }}
        />
      </div>
    );
  }
}

Card.propTypes = {
  header: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  article: PropTypes.string.isRequired,
  color: PropTypes.string,
  addInCart: PropTypes.func.isRequired,
  addInFav: PropTypes.func.isRequired,
  cardsInCart: PropTypes.array.isRequired,
  cardsInFav: PropTypes.array.isRequired
};

Card.defaultProps = {
  color: "black",
};

export default Card;
