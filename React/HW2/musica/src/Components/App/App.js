import style from "./App.module.scss";
import Button from "../Button";
import Modal from "../Modal";
import { Component } from "react";
import CardList from "../CardList";
import Header from "../Header/Header";

class App extends Component {
  state = {
    isModalOpen: false,
    cards: [],
    cardsInCart: [],
    cardsInFav: [],
    art: 0,
  };

  async componentDidMount() {
    const cardsInCart = localStorage.getItem("cardsInCart");
    const cardsInFav = localStorage.getItem("cardsInFav");
    if (cardsInCart) {
      this.setState({ cardsInCart: JSON.parse(cardsInCart) });
    }
    if (cardsInFav) {
      this.setState({ cardsInFav: JSON.parse(cardsInFav) });
    }
    const cards = await fetch("./data.json").then((res) => res.json());
    this.setState({ cards: cards });
  }

  ModalToggleClick = () => {
    this.setState((prevState) => ({
      isModalOpen: !prevState.isModalOpen,
    }));
  };

  startCartAdded = (article) => {
    this.setState({art: article})
    this.ModalToggleClick();
  }

  AddInCart = () => {
    const article = this.state.art
    if (this.state.cardsInCart.includes(article)) {
      this.setState({
        cardsInCart: this.state.cardsInCart.splice(
          this.state.cardsInCart.indexOf(article),
          1
        ),
      });
    } else {
      this.setState({
        cardsInCart: this.state.cardsInCart.push(article),
      });
    }
    this.ModalToggleClick();
    this.setState({
      cardsInCart: this.state.cardsInCart,
    });
    localStorage.setItem("cardsInCart", JSON.stringify(this.state.cardsInCart));
  };

  AddInFav = (article) => {
    if (this.state.cardsInFav.includes(article)) {
      this.setState({
        cardsInFav: this.state.cardsInFav.splice(
          this.state.cardsInFav.indexOf(article),
          1
        ),
      });
    } else {
      this.setState({
        cardsInFav: this.state.cardsInFav.push(article),
      });
    }
    this.setState({
      cardsInFav: this.state.cardsInFav,
    });
    localStorage.setItem("cardsInFav", JSON.stringify(this.state.cardsInFav));
  };

  render() {
    const { isModalOpen, cards, cardsInCart, cardsInFav} = this.state;
    return (
      <div className={style.App}>
        <Header quantityCart={cardsInCart.length} quantintyFav={cardsInFav.length}></Header>
        <CardList
          cards={cards}
          addInCart={this.startCartAdded}
          cardsInCart={cardsInCart}
          addInFav = {this.AddInFav}
          cardsInFav={cardsInFav}
        ></CardList>
        {isModalOpen && (
          <Modal
            header="Changes in Cart"
            closeButton={true}
            text="you real want to do this?"
            closeModal={this.ModalToggleClick}
            actions={
              <>
                <Button
                  className={style.btn}
                  backgroundColor="black"
                  text="Cancel"
                  onClick={this.ModalToggleClick}
                />
                <Button
                  className={style.btn}
                  backgroundColor="black"
                  text="Ok"
                  onClick={this.AddInCart}
                />
              </>
            }
          ></Modal>
        )}
      </div>
    );
  }
}
export default App;
