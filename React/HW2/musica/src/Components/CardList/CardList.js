import { Component } from "react";
import style from "./CardList.module.scss";
import Card from "../Card";
import PropTypes from "prop-types";

class CardList extends Component {
  render() {
    const { cards, addInCart, cardsInCart, cardsInFav, addInFav } = this.props;
    return (
      <ul className={style.CardList}>
        {cards.map(({ header, src, price, article, color, inCart }) => (
          <li key={article}>
            <Card
              header={header}
              src={src}
              price={price}
              color={color}
              inCart={inCart}
              addInCart={addInCart}
              article={article}
              addInFav={addInFav}
              cardsInFav={cardsInFav.includes(article)}
              cardsInCart={cardsInCart.includes(article)}
            ></Card>
          </li>
        ))}
      </ul>
    );
  }
}

Card.propTypes = {
  cards: PropTypes.array.isRequired,
  addInCart: PropTypes.func.isRequired,
  addInFav: PropTypes.func.isRequired,
  cardsInCart: PropTypes.array.isRequired,
  cardsInFav: PropTypes.array.isRequired
};

export default CardList;
