import style from "./Button.module.scss";
import { Component } from "react";
import PropTypes from "prop-types";

class Button extends Component {
  render() {
    const { backgroundColor, text, onClick, className } = this.props;

    return (
      <button
        className={className}
        style={{ backgroundColor: backgroundColor }}
        onClick={onClick}
      >
        {text}
      </button>
    );
  }
}

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
  backgroundColor: "black",
};

export default Button;
