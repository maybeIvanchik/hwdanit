import PropTypes from "prop-types";

const Button = ({ backgroundColor, text, onClick, className }) => {
  return (
    <button
      className={className}
      style={{ backgroundColor: backgroundColor }}
      onClick={onClick}
    >
      {text}
    </button>
  );
};

Button.propTypes = {
  backgroundColor: PropTypes.string,
  text: PropTypes.string.isRequired,
  onClick: PropTypes.func.isRequired,
};

Button.defaultProps = {
  backgroundColor: "black",
};

export default Button;
