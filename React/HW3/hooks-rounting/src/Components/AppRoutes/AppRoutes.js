import { Routes, Route } from "react-router-dom";
import React from "react";
import Homepage from "../../pages/Homepage";
import Cartpage from "../../pages/Cartpage";
import Favpage from "../../pages/Favpage";
import PropTypes from "prop-types";

const AppRoutes = (props) => {
  const {
    cards,
    startCartAdded,
    cardsInCart,
    AddInCart,
    AddInFav,
    cardsInFav,
    ModalClickToggle,
    isModalOpen,
    deleteFromCart,
    setCardsInCart
  } = props;
  return (
    <>
      <Routes>
        <Route
          path="/"
          element={
            <Homepage
              cards={cards}
              startCartAdded={startCartAdded}
              cardsInCart={cardsInCart}
              AddInCart={AddInCart}
              AddInFav={AddInFav}
              cardsInFav={cardsInFav}
              ModalClickToggle={ModalClickToggle}
              isModalOpen={isModalOpen}
            />
          }
        />
        <Route
          path="/cart"
          element={
            <Cartpage
              cards={cards}
              cardsInCart={cardsInCart}
              addInCart={deleteFromCart}
              startCartAdded={startCartAdded}
              ModalClickToggle={ModalClickToggle}
              isModalOpen={isModalOpen}
              setCardsInCart={setCardsInCart}
            />
          }
        />
        <Route
          path="/fav"
          element={
            <Favpage
              cards={cards}
              AddInFav={AddInFav}
              cardsInFav={cardsInFav}
            />
          }
        />
      </Routes>
    </>
  );
};

AppRoutes.propTypes = {
  cards: PropTypes.array.isRequired,
  startCartAdded: PropTypes.func.isRequired,
  cardsInCart: PropTypes.array.isRequired,
  AddInCart: PropTypes.func.isRequired,
  AddInFav: PropTypes.func.isRequired,
  cardsInFav: PropTypes.array.isRequired,
  ModalClickToggle: PropTypes.func.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  deleteFromCart: PropTypes.func.isRequired,
  setCardsInCart: PropTypes.func.isRequired
};

export default AppRoutes;
