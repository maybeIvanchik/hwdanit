import style from "./CartList.module.scss";
import Cart from "../Cart/Cart";
import PropTypes from "prop-types";

const CartList = (props) => {
  const { cards, cardsInCart, addInCart, setCardsInCart } = props;
  if (cardsInCart.length === 0) {
    return (
      <h1 className={style.title}>
        There are no items added in your cart yet.
      </h1>
    );
  }
  const cartCountChanging = (article, mode, cardsInCart) => {
    if (mode === "+") {
      const newCart = [...cardsInCart, article];
      setCardsInCart(newCart);
      localStorage.setItem("cardsInCart", JSON.stringify(newCart));
      return newCart;
    } else {
      const newCart = cardsInCart.filter(
        (item, index) => index !== cardsInCart.indexOf(article)
      );
      setCardsInCart(newCart);
      localStorage.setItem("cardsInCart", JSON.stringify(newCart));
      return newCart;
    }
  };
  return (
    <ul className={style.CartList}>
      {cards.map(({ header, src, price, article, color, inCart }) => {
        if (cardsInCart.includes(article)) {
          return (
            <li key={article}>
              <Cart
                header={header}
                src={src}
                price={price}
                color={color}
                inCart={inCart}
                article={article}
                cardsInCart={cardsInCart}
                addInCart={addInCart}
                cartCountChanging={cartCountChanging}
              ></Cart>
              <div className={style.count}>
                <button
                  className={style.counter}
                  onClick={() => cartCountChanging(article, "-", cardsInCart)}
                >
                  -
                </button>
                <p className={style.priceTitle}>{cardsInCart.filter(item => item === article).length}</p>
                <button
                  className={style.counter}
                  onClick={() => cartCountChanging(article, "+", cardsInCart)}
                >
                  +
                </button>
              </div>
            </li>
          );
        }
      })}
    </ul>
  );
};

CartList.propTypes = {
  cards: PropTypes.array.isRequired,
  cardsInCart: PropTypes.arrayOf(PropTypes.string).isRequired,
  addInCart: PropTypes.func.isRequired,
  setCardsInCart: PropTypes.func.isRequired
};

export default CartList;
