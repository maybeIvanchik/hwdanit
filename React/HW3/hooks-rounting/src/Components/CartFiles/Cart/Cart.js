import style from "./Cart.module.scss";
import Button from "../../Button";
import PropTypes from "prop-types";

const Cart = (props) => {
  const { header, src, price, article, color, addInCart, cardsInCart } = props;
  const countOfCarts = cardsInCart.filter(item => item === article).length
  const rightPrice = price.split("$")[1] * countOfCarts
  return (
    <div className={style.Cart}>
      <img
        className={style.img}
        src={src}
        alt="img"
        width="250px"
        height="250px"
      />
      <h2 className={style.title}>{header}</h2>
      <p className={style.price}>${rightPrice}</p>
      <div className={style.color} style={{ backgroundColor: color }}></div>
      <div>
        <Button
          backgroundColor={"#ea2f2f"}
          text={"Delete from Cart"}
          className={style.btn}
          onClick={() => {
            addInCart(article);
          }}
        />
      </div>
    </div>
  );
};

Cart.propTypes = {
  header: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  price: PropTypes.string.isRequired,
  article: PropTypes.string.isRequired,
  color: PropTypes.string.isRequired,
  addInCart: PropTypes.func.isRequired,
  cardsInCart: PropTypes.array.isRequired
};

export default Cart;
