import { NavLink } from "react-router-dom";
import style from "./Navbar.module.scss";

const Navbar = () => {
  return (
    <nav className={style.nav}>
      <NavLink to="/">
        <button className={style.btn}>Home</button>
      </NavLink>
      <NavLink to="/cart">
        <button className={style.btn}>Cart</button>
      </NavLink>
      <NavLink to="/fav">
        <button className={style.btn}>Fav</button>
      </NavLink>
    </nav>
  );
};
export default Navbar;
