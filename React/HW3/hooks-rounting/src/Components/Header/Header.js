import style from "./Header.module.scss";
import PropTypes from "prop-types";

const Header = ({ quantintyFav, quantityCart }) => {
  return (
    <section className={style.Header}>
      <div className={style.Box}>
        <img
          src="./img/icon/shopping-basket.png"
          alt="cart"
          width="40px"
          height="40px"
        />
        <p className={style.textCart}>{quantityCart}</p>
      </div>
      <div className={style.Box}>
        <svg
          className={style.star}
          xmlns="http://www.w3.org/2000/svg"
          version="1.1"
          width="20px"
          height="20px"
          fill="#ea2f2f"
        >
          <polygon points="12,3 6,21 21,9 3,9 18,21" />
        </svg>
        <p className={style.textFav}>{quantintyFav}</p>
      </div>
    </section>
  );
};

Header.propTypes = {
  quantintyFav: PropTypes.number.isRequired,
  quantityCart: PropTypes.number.isRequired,
};

export default Header;
