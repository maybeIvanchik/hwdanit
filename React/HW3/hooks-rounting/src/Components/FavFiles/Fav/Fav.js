import style from "./Fav.module.scss";
import Button from "../../Button";
import PropTypes from 'prop-types';

const Fav = (props) => {
  const { header, src, article, addInFav } = props;
  return (
    <div className={style.Fav}>
      <img
        className={style.img}
        src={src}
        alt="img"
        width="250px"
        height="250px"
      />
      <h2 className={style.title}>{header}</h2>
      <div>
        <Button
          backgroundColor={"#ea2f2f"}
          text={"Delete from Fav"}
          className={style.btn}
          onClick={() => {
            addInFav(article);
          }}
        />
      </div>
    </div>
  );
};

Fav.propTypes = {
  header: PropTypes.string.isRequired,
  src: PropTypes.string.isRequired,
  article: PropTypes.object.isRequired,
  addInFav: PropTypes.func.isRequired,
};

export default Fav;