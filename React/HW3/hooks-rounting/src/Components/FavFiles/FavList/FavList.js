import style from "./FavList.module.scss";
import Fav from "../Fav";
import PropTypes from 'prop-types';

const FavList = (props) => {
    const {cards, cardsInFav, addInFav} = props
    if (cardsInFav.length === 0) {
        return (
          <h1 className={style.title}>
            There are no items added in your cart yet.
          </h1>
        );
      }
      return (
        <ul className={style.FavList}>
          {cards.map(({ header, src, article }) => {
            if (cardsInFav.includes(article)) {
              return (
                <li key={article}>
                  <Fav
                    header={header}
                    src={src}
                    article={article}
                    addInFav={addInFav}
                  ></Fav>
                </li>
              );
            }
          })}
        </ul>
      );
}

FavList.propTypes = {
  cards: PropTypes.array.isRequired,
  cardsInFav: PropTypes.arrayOf(PropTypes.string).isRequired,
  addInFav: PropTypes.func.isRequired
};

export default FavList