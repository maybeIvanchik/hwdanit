import style from "./CardList.module.scss";
import Card from "../Card";
import PropTypes from "prop-types";

const CardList = (props) => {
  const { cards, addInCart, cardsInFav, addInFav } = props;
  return (
    <ul className={style.CardList}>
      {cards.map(({ header, src, price, article, color, inCart }) => (
        <li key={article}>
          <Card
            header={header}
            src={src}
            price={price}
            color={color}
            inCart={inCart}
            addInCart={addInCart}
            article={article}
            addInFav={addInFav}
            cardsInFav={cardsInFav.includes(article)}
          ></Card>
        </li>
      ))}
    </ul>
  );
};

Card.propTypes = {
  cards: PropTypes.array,
  addInCart: PropTypes.func.isRequired,
  addInFav: PropTypes.func.isRequired,
  cardsInFav: PropTypes.bool.isRequired,
};

export default CardList;
