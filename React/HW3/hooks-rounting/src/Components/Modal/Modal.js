import style from "./Modal.module.scss";
import PropTypes from "prop-types";

const Modal = (props) => {
  const {header, closeButton, text, actions, closeModal} = props;

  return (
    <>
      <div className={style.Modal}>
        <h2 className={style.title}>{header}</h2>
        <p className={style.desc}>{text}</p>
        {closeButton && (
          <div className={style.closeBtn} onClick={closeModal}></div>
        )}
        {actions}
      </div>
      <div className={style.back} onClick={closeModal}></div>
    </>
  );
};

Modal.propTypes = {
  header: PropTypes.string.isRequired,
  closeButton: PropTypes.bool,
  text: PropTypes.string.isRequired,
  actions: PropTypes.node,
  closeModal: PropTypes.func.isRequired,
};

Modal.defaultProps = {
  closeButton: false,
  actions: "",
};

export default Modal;
