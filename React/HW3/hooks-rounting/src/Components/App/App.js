import style from "./App.module.scss";
import Header from "../Header/Header";
import { useState, useEffect } from "react";
import AppRoutes from "../AppRoutes";
import Navbar from "../Navbar/Navbar";

const App = () => {
  const [isModalOpen, setIsModalOpen] = useState(false);
  const [cards, setCards] = useState([]);
  const [cardsInCart, setCardsInCart] = useState([]);
  const [cardsInFav, setCardsInFav] = useState([]);
  const [art, setArt] = useState([]);

  useEffect(() => {
    const cardsInCart = localStorage.getItem("cardsInCart");
    const cardsInFav = localStorage.getItem("cardsInFav");
    if (cardsInCart) {
      setCardsInCart(JSON.parse(cardsInCart));
    }
    if (cardsInFav) {
      setCardsInFav(JSON.parse(cardsInFav));
    }
    const fetchData = async () => {
      const cards = await fetch("./data.json").then((res) => res.json());
      setCards(cards);
    };
    fetchData();
  }, []);

  const ModalClickToggle = () => {
    setIsModalOpen((prevState) => !prevState);
  };

  const startCartAdded = (article) => {
    setArt(article);
    ModalClickToggle();
  };

  const AddInCart = () => {
    const article = art;
    setCardsInCart((prevCart) => {
      const newCart =[...prevCart, article];
      localStorage.setItem("cardsInCart", JSON.stringify(newCart));
      return newCart;
    });
    ModalClickToggle();
  };
  // setCardsInCart((prevFav) => {
  //   const newCart = prevFav.includes(article)
  //     ? prevFav.filter((item) => item !== article)
  //     : [...prevFav, article];
  //   localStorage.setItem("cardsInCart", JSON.stringify(newCart));
  //   return newCart;
  // });
  const deleteFromCart = () => {
     const article = art;
    setCardsInCart((prevCart) => {
      const newCart = prevCart.filter((item) => item !== article)
      localStorage.setItem("cardsInCart", JSON.stringify(newCart));
      return newCart;
    })
    ModalClickToggle();
  }

  const AddInFav = (article) => {
    setCardsInFav((prevFav) => {
      const newFav = prevFav.includes(article)
        ? prevFav.filter((item) => item !== article)
        : [...prevFav, article];
      localStorage.setItem("cardsInFav", JSON.stringify(newFav));
      return newFav;
    });
  };
  return (
    <div className={style.App}>
      <Header
        quantityCart={cardsInCart.length}
        quantintyFav={cardsInFav.length}
      />
      <AppRoutes
        cards={cards}
        startCartAdded={startCartAdded}
        cardsInCart={cardsInCart}
        AddInCart={AddInCart}
        AddInFav={AddInFav}
        cardsInFav={cardsInFav}
        ModalClickToggle={ModalClickToggle}
        isModalOpen={isModalOpen}
        deleteFromCart={deleteFromCart}
        setCardsInCart={setCardsInCart}
      />
      <Navbar />
    </div>
  );
};
export default App;
