import style from "../Pages.module.scss";
import Button from "../../Components/Button";
import Modal from "../../Components/Modal";
import CardList from "../../Components/CardFiles/CardList";

const Homepage = (props) => {
  const {
    cards,
    startCartAdded,
    cardsInCart,
    AddInCart,
    AddInFav,
    cardsInFav,
    ModalClickToggle,
    isModalOpen,
  } = props;
  return (
    <>
      <CardList
        cards={cards}
        addInCart={startCartAdded}
        cardsInCart={cardsInCart}
        addInFav={AddInFav}
        cardsInFav={cardsInFav}
      />
      {isModalOpen && (
        <Modal
          header="Changes in Cart"
          closeButton={true}
          text="you real want to do this?"
          closeModal={ModalClickToggle}
          actions={
            <>
              <Button
                className={style.btn}
                backgroundColor="black"
                text="Cancel"
                onClick={ModalClickToggle}
              />
              <Button
                className={style.btn}
                backgroundColor="black"
                text="Ok"
                onClick={AddInCart}
              />
            </>
          }
        />
      )}
    </>
  );
};

export default Homepage;
