import FavList from "../../Components/FavFiles/FavList"

const FavPage = (props) => {
  const { cards, AddInFav, cardsInFav} = props;
  return (
    <>
      <FavList
        cards={cards}
        addInFav={AddInFav}
        cardsInFav={cardsInFav}
      />
    </>
  );
};

export default FavPage