import style from "../Pages.module.scss";
import CartList from "../../Components/CartFiles/CartList/CartList";
import Modal from "../../Components/Modal";
import Button from "../../Components/Button";

const Cartpage = (props) => {
  const { cards, cardsInCart, isModalOpen, startCartAdded, addInCart, ModalClickToggle, setCardsInCart} =
    props;
  return (
    <>
      <CartList cards={cards} cardsInCart={cardsInCart} addInCart={startCartAdded} setCardsInCart={setCardsInCart} />
      {isModalOpen && (
        <Modal
          header="Changes in Cart"
          closeButton={true}
          text="you real want to delete this?"
          closeModal={ModalClickToggle}
          actions={
            <>
              <Button
                className={style.btn}
                backgroundColor="black"
                text="Cancel"
                onClick={ModalClickToggle}
              />
              <Button
                className={style.btn}
                backgroundColor="black"
                text="Ok"
                onClick={addInCart}
              />
            </>
          }
        />
      )}
    </>
  );
};

export default Cartpage;
