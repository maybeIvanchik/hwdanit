import style from "./Modal.module.scss";
import { Component } from "react";

class Modal extends Component {
  render() {
    const { header, closeButton, text, actions, closeModal } = this.props;

    return (
      <>
        <div className={style.Modal}>
          <h2 className={style.title}>{header}</h2>
          <p className={style.desc}>{text}</p>
          {closeButton && <div className={style.closeBtn} onClick={closeModal}></div>}
          {actions}
        </div>
        <div className={style.back} onClick={closeModal}></div>
      </>
    );
  }
}

export default Modal;
