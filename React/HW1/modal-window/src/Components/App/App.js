import style from "./App.module.scss";
import Button from "../Button";
import Modal from "../Modal";
import { Component } from "react";

class App extends Component {
  state = {
    isFirstModalOpen: false,
    isSecondModalOpen: false,
  };

  firstModalToggle = () => {
    this.setState((prevState) => ({
      isFirstModalOpen: !prevState.isFirstModalOpen,
    }));
  };
  secondModalToggle = () => {
    this.setState((prevState) => ({
      isSecondModalOpen: !prevState.isSecondModalOpen,
    }));
  };
  render() {
    const { isFirstModalOpen, isSecondModalOpen } = this.state;
    return (
      <div className={style.App}>
        <Button
          className={style.btn}
          text="Open first modal"
          backgroundColor="red"
          onClick={this.firstModalToggle}
        ></Button>
        <Button
          className={style.btn}
          text="Open second modal"
          backgroundColor="black"
          onClick={this.secondModalToggle}
        ></Button>
        {isFirstModalOpen && (
          <Modal
            header="Hi guys we have a gift for you?"
            closeButton={true}
            text="if you want to make it choose yes or no"
            closeModal={this.firstModalToggle}
            actions={
              <>
                <Button
                  className={style.btn}
                  backgroundColor="black"
                  text="Cancel"
                  onClick={this.firstModalToggle}
                />
                <Button
                  className={style.btn}
                  backgroundColor="black"
                  text="Ok"
                  onClick={this.firstModalToggle}
                />
              </>
            }
          ></Modal>
        )}
        {isSecondModalOpen && (
          <Modal
            header="Do you want to travel Ohaio?"
            closeButton={true}
            text="all answer its yes, even if no"
            closeModal={this.secondModalToggle}
            actions={
              <>
                <Button
                  className={style.btn}
                  backgroundColor="black"
                  text="Cancel"
                  onClick={this.secondModalToggle}
                />
                <Button
                  className={style.btn}
                  backgroundColor="black"
                  text="Ok"
                  onClick={this.secondModalToggle}
                />
              </>
            }
          ></Modal>
        )}
      </div>
    );
  }
}

export default App;
